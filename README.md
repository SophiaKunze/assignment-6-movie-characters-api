# Assignment 6: Create a Web API and database with Spring

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Java](https://img.shields.io/badge/-Java-red?logo=java)](https://www.java.com)
[![Spring](https://img.shields.io/badge/-Spring-white?logo=spring)](https://spring.io/)

This application is the third assignment of the backend part of the [Noroff](https://www.noroff.no/en/) Full-Stack developer course.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

The tasks was to create a PostgreSQL database using Hibernate and to expose it through a deployed Web API.
It consists of a datastore and interface to store and manipulate movie characters. 
It is designed in a way that it may be expanded over time to include other digital media.
The application is constructed in Spring Web and comprises a database made in PostgreSQL through Hibernate with 
a RESTful API to allow users to manipulate the data. 
The database will stores information about characters, movies they appear in, and the franchises these movies belong to.

## Install

This project was generated with OpenJDK version 17.0.2 through Spring Initializr and Gradle build system.
Clone repository via `git clone`. 
Use the Web API through [Swagger](https://sk-movie-characters.herokuapp.com/swagger-ui/index.html).

## Maintainers

[@SophiaKunze](https://gitlab.com/SophiaKunze)

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/SophiaKunze/assignment-6-movie-characters-api/-/issues/new). 
This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

### Acknowledgement
This project exists thanks to my teacher <a href="https://gitlab.com/NicholasLennox">Nicholas Lennox</a> and <a href=https://www.experis.de/de>Experis</a>.

## License

MIT © 2022 Sophia Kunze
