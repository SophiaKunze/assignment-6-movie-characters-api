-- +++++++++++++ franchise table +++++++++++++
-- required columns only
INSERT INTO franchise ("name") VALUES ('Marvel Cinematic Universe');
-- all columns
INSERT INTO franchise ("name", "description") VALUES ('Star Wars', 'Star Wars is an American epic space-opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon.');
INSERT INTO franchise ("name", description) VALUES ('Harry Potter', 'Harry Potter is a film series based on the eponymous novels by J. K. Rowling. The series is distributed by Warner Bros. and consists of eight fantasy films.');

-- +++++++++++++ movie table +++++++++++++
-- all columns
INSERT INTO movie ("title", "director_name", "genre", "poster_url", "trailer_url", "year", franchise_id) VALUES ('Harry Potter and the Philosophers Stone', 'Chris Columbus', 'fantasy', 'https://m.media-amazon.com/images/I/815v2OuIHXL._SL1500_.jpg', 'https://www.youtube.com/watch?v=VyHV0BRtdxo', 2001, 3);
-- some optional columns
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Chamber of Secrets', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Prisoner of Azkaban', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Goblet of Fire', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Order of the Phoenix', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Half-Blood Prince', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Deathly Hallows Part 1', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Harry Potter and the Deathly Hallows Part 2', 3);
INSERT INTO movie ("title", franchise_id) VALUES ('Iron Man', 1);
INSERT INTO movie ("title", franchise_id) VALUES ('The Incredible Hulk', 1);
-- required columns only
INSERT INTO movie ("title") VALUES ('Iron Man 2');
INSERT INTO movie ("title") VALUES ('Captain America: The First Avenger');
INSERT INTO movie ("title") VALUES ('Marvels The Avengers');

-- +++++++++++++ character table +++++++++++++
-- all columns
INSERT INTO character ("name", alias, "gender", "picture_url") VALUES ('Harry Potter', 'Harry', 'male', 'https://crops.giga.de/7b/52/f1/034f6acba8446a6277ebd554d9_YyAxNTY2eDg4MSsyKzEzMAJyZSA4NDAgNDcyAzcyMGZjODYyZTVk.jpg');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Ronald Weasley', 'Ron', 'male', 'https://images4.fanpop.com/image/photos/16500000/Ronald-Weasley-ronald-weasley-16503004-1699-2100.jpg');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Hermine Granger', 'Hermine', 'female', 'https://imgix.bustle.com/rehost/2016/9/13/33785d76-556f-4ea4-8cf3-10d190897860.png?w=614&fit=crop&crop=faces&auto=format%2Ccompress&q=50&dpr=2');
-- some optional columns
INSERT INTO character ("name", "alias") VALUES ('Fleur Delacour', 'Fleur');
INSERT INTO character ("name", "gender") VALUES ('Viktor Krum', 'male');
-- required columns only
INSERT INTO character ("name") VALUES ('Cedric Diggory');

-- +++++++++++++ movie_characters table +++++++++++++
INSERT INTO character_movies (characters_id, movies_id) VALUES (1, 1);
INSERT INTO character_movies (characters_id, movies_id) VALUES (1, 2);
INSERT INTO character_movies (characters_id, movies_id) VALUES (1, 3);
INSERT INTO character_movies (characters_id, movies_id) VALUES (2, 1);
INSERT INTO character_movies (characters_id, movies_id) VALUES (3, 1);
