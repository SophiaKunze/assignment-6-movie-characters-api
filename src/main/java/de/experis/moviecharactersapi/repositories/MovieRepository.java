package de.experis.moviecharactersapi.repositories;

import de.experis.moviecharactersapi.models.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
