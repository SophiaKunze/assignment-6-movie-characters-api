package de.experis.moviecharactersapi.services;

import java.util.Collection;

public interface CrudService<T, ID> {
    // Generic CRUD
    T findById(ID id); // GET

    Collection<T> findAll(); // GET

    T add(T entity); // POST

    T update(T entity); // PUT

    void deleteById(ID id); // DELETE
}
