package de.experis.moviecharactersapi.services.character;

import de.experis.moviecharactersapi.exceptions.CharacterNotFoundException;
import de.experis.moviecharactersapi.exceptions.MovieNotFoundException;
import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

/**
 * Implementation of the Character service.
 * Uses the Character repository to interact with the data store.
 */
@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    /**
     * Gets a character by character's id.
     * @param id of the character
     * @return the character
     * @throws CharacterNotFoundException if character with given id does not exist in the data store
     */
    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException(id));
    }

    /**
     * Gets all characters.
     * @return the characters
     */
    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    /**
     * Adds a new character to the data store.
     * Character's movies will be null.
     * @param character to be added
     * @return the saved character
     */
    @Override
    public Character add(Character character) {
        // Set related data to null (business logic).
        character.setMovies(null);
        return characterRepository.save(character);
    }

    /**
     * Updates an existing character.
     * Movies of this character will not change.
     * @param character with updated fields
     * @return the updated character
     * @throws CharacterNotFoundException if character does not exist by its id in the data store
     */
    @Override
    public Character update(Character character) {
        int characterId = character.getId();
         // Receive movies of character.
        Set<Movie> movies = characterRepository
                .findById(characterId)
                // Make sure given character exists by id.
                .orElseThrow(() -> new CharacterNotFoundException(characterId))
                .getMovies();
        // Set movies to old value (business logic).
        character.setMovies(movies);
        return characterRepository.save(character);
    }

    /**
     * Deletes a character by its id.
     * @param id of the character
     * @throws CharacterNotFoundException if character with given id does not exist in the data store
     */
    @Override
    public void deleteById(Integer id) {
        // obtain character
        Character character = characterRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException(id));
        // delete character
        characterRepository.delete(character);
    }
}
