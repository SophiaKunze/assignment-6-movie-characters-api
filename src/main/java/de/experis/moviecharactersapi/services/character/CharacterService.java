package de.experis.moviecharactersapi.services.character;

import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.services.CrudService;

public interface CharacterService extends CrudService<Character, Integer> {
}
