package de.experis.moviecharactersapi.services.franchise;

import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Franchise;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.services.CrudService;

import java.util.Set;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    Franchise updateMovies(int[] movieIds, int franchiseId);

    Set<Movie> findAllMovies(int franchiseId);

    Set<Character> findAllCharacters(int franchiseId);
}
