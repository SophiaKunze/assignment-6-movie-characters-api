package de.experis.moviecharactersapi.services.franchise;

import de.experis.moviecharactersapi.exceptions.CharacterNotFoundException;
import de.experis.moviecharactersapi.exceptions.FranchiseNotFoundException;
import de.experis.moviecharactersapi.exceptions.MovieNotFoundException;
import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Franchise;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.repositories.FranchiseRepository;
import de.experis.moviecharactersapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of the Franchise service.
 * Uses the Franchise repository and Movie repository to interact with the data store.
 */
@Service
public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * Gets a franchise by franchise's id.
     * @param id of the franchise
     * @return the franchise
     * @throws FranchiseNotFoundException if franchise with given id does not exist in the data store
     */
    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    /**
     * Gets all franchises.
     * @return the franchises
     */
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    /**
     * Adds a new franchise to the data store.
     * Franchise's movies will be null.
     * @param franchise to be added
     * @return the saved franchise
     */
    @Override
    public Franchise add(Franchise franchise) {
        // Set related data to null (business logic).
        franchise.setMovies(null);
        return franchiseRepository.save(franchise);
    }

    /**
     * Updates an existing franchise.
     * Movies of this franchise will not change.
     * @param franchise with updated fields
     * @return the updated franchise
     * @throws FranchiseNotFoundException if franchise does not exist by its id in the data store
     */
    @Override
    public Franchise update(Franchise franchise) {
        // check if franchise exists in data store
        int franchiseId = franchise.getId();
        if (!franchiseRepository.existsById(franchiseId))
            throw new FranchiseNotFoundException(franchiseId);
        return franchiseRepository.save(franchise);
    }

    /**
     * Deletes a franchise by its id.
     * @param id of the franchise
     * @throws FranchiseNotFoundException if franchise with given id does not exist in the data store
     */
    @Override
    @Transactional
    public void deleteById(Integer id) {
        // obtain franchise
        Franchise franchise = franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
        // set relationships to null so that referential deletion is possible
        franchise.getMovies().forEach(movie -> movie.setFranchise(null));
        // delete franchise
        franchiseRepository.delete(franchise);
    }

    /**
     * Updates movies of a franchise. This will overwrite current movies.
     * @param movieIds new movies
     * @param franchiseId of the franchise to be updated
     * @return the updated franchise
     * @throws FranchiseNotFoundException if franchise with given id does not exist in the data store
     */
    @Override
    public Franchise updateMovies(int[] movieIds, int franchiseId) {
        // Receive franchise.
        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        for (int id : movieIds) {
            if (movieRepository.existsById(id)) {
                // Receive movies by ids.
                Movie movie = movieRepository.findById(id)
                        .orElseThrow(() -> new MovieNotFoundException(id));
                // Update franchise of movie. Do NOT set movie of a franchise, since movie holds relationship!
                movie.setFranchise(franchise);
                movieRepository.save(movie);
            }
        }
        return franchise;
    }

    /**
     * Gets all movies of a franchise.
     * @param franchiseId of the franchise whose movies are to be found
     * @return movies of the franchise
     * @throws FranchiseNotFoundException if franchise with given id does not exist in the data store
     */
    @Override
    public Set<Movie> findAllMovies(int franchiseId) {
        // access franchise
        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        // access movies of given franchise
        return franchise.getMovies();
    }

    /**
     * Gets all characters of a franchise.
     * @param franchiseId of the franchise whose characters are to be found
     * @return characters of the franchise
     * @throws FranchiseNotFoundException if franchise with given id does not exist in the data store
     */
    @Override
    public Set<Character> findAllCharacters(int franchiseId) {
        // access franchise
        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        // access all movies
        Set<Movie> movies = franchise.getMovies();
        // for each movie, access characters
        return movies.stream()
                .flatMap(m -> m.getCharacters().stream())
                .collect(Collectors.toSet());
    }
}
