package de.experis.moviecharactersapi.services.movie;

import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.services.CrudService;

import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    Movie updateCharacters(int[] characterIds, int movieId);

    Set<Character> findAllCharacters(int movieId);
}
