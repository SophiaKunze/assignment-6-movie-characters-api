package de.experis.moviecharactersapi.services.movie;

import de.experis.moviecharactersapi.exceptions.CharacterNotFoundException;
import de.experis.moviecharactersapi.exceptions.FranchiseNotFoundException;
import de.experis.moviecharactersapi.exceptions.MovieNotFoundException;
import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.repositories.CharacterRepository;
import de.experis.moviecharactersapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the Movie service.
 * Uses the Movie repository and Character repository to interact with the data store.
 */
@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    /**
     * Gets a movie by movie's id.
     * @param id of the movie
     * @return the movie
     * @throws MovieNotFoundException if movie with given id does not exist in the data store
     */
    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    /**
     * Gets all movies.
     * @return the movies
     */
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    /**
     * Adds a new movie to the data store.
     * Movie's franchise and characters will be null.
     * @param movie to be added
     * @return the saved movie
     */
    @Override
    public Movie add(Movie movie) {
        // Set related data to null (business logic).
        movie.setFranchise(null);
        movie.setCharacters(null);
        return movieRepository.save(movie);
    }

    /**
     * Updates an existing movie.
     * Characters of this movie will not change.
     * The franchise will be updated.
     * @param movie with updated fields
     * @return the updated movie
     * @throws MovieNotFoundException if movie does not exist by its id in the data store
     */
    @Override
    public Movie update(Movie movie) {
        // Make sure given movie exists by id.
        int movieId = movie.getId();
        if (!movieRepository.existsById(movieId)) {
            throw new MovieNotFoundException(movieId);
        }
        return movieRepository.save(movie);
    }

    /**
     * Deletes a movie by its id.
     * @param id of the movie
     * @throws MovieNotFoundException if movie with given id does not exist in the data store
     */
    @Override
    public void deleteById(Integer id) {
        // obtain movie
        Movie movie = movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
        // delete movie from relationship to character
        movie.getCharacters()
                .forEach(character -> character.getMovies().remove(movie));
        // delete movie
        movieRepository.delete(movie);
    }

    /**
     * Updates characters of a movie. This will overwrite current characters.
     * @param characterIds new characters
     * @param movieId of the movie to be updated
     * @return the updated movie
     * @throws MovieNotFoundException if movie with given id does not exist in the data store
     */
    @Override
    public Movie updateCharacters(int[] characterIds, int movieId) {
        // Receive movie.
        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new MovieNotFoundException(movieId));
        Set<Character> characters = new HashSet<>();
        for (int id : characterIds) {
            // Add character to characters.
            characters.add(characterRepository.findById(id)
                    .orElseThrow(() -> new CharacterNotFoundException(id)));
        }
        movie.setCharacters(characters);
        return movieRepository.save(movie);
    }

    /**
     * Gets all characters of a movie.
     * @param movieId of the movie whose characters are to be found
     * @return characters of the movie
     * @throws MovieNotFoundException if movie with given id does not exist in the data store
     */
    @Override
    public Set<Character> findAllCharacters(int movieId) {
        // access movie
        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new MovieNotFoundException(movieId));
        // access characters of given movie
        return movie.getCharacters();
    }
}
