package de.experis.moviecharactersapi.runners;

import de.experis.moviecharactersapi.services.character.CharacterService;
import de.experis.moviecharactersapi.services.franchise.FranchiseService;
import de.experis.moviecharactersapi.services.movie.MovieService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

    private final CharacterService characterService;
    private final MovieService movieService;
    private final FranchiseService franchiseService;

    // constructor injected fields
    public AppRunner(CharacterService characterService, MovieService movieService, FranchiseService franchiseService) {
        this.characterService = characterService;
        this.movieService = movieService;
        this.franchiseService = franchiseService;
    }

    @Override
    public void run(ApplicationArguments arg) throws Exception {
    }
}
