package de.experis.moviecharactersapi.controllers;

import de.experis.moviecharactersapi.mappers.*;
import de.experis.moviecharactersapi.models.domain.Franchise;
import de.experis.moviecharactersapi.models.dto.*;
import de.experis.moviecharactersapi.services.franchise.FranchiseService;

import de.experis.moviecharactersapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all franchises.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseGetDTO.class)))})
    })
    @GetMapping // GET: localhost:8080/api/v1/franchises
    public ResponseEntity<Collection<FranchiseGetDTO>> findAll() {
        Collection<FranchiseGetDTO> franchises = franchiseMapper.franchiseToFranchiseGetDto(franchiseService.findAll());
        return ResponseEntity.ok(franchises);
    }

    @Operation(summary = "Get a franchise by ID.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = FranchiseGetDTO.class))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/franchises/1
    public ResponseEntity<FranchiseGetDTO> findById(@PathVariable int id) {
        FranchiseGetDTO franchise = franchiseMapper.franchiseToFranchiseGetDto(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "Get movies of a franchise.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MovieGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{franchiseId}/movies") // GET: localhost:8080/api/v1/franchises/1/movies
    public ResponseEntity<Collection<MovieGetDTO>> findAllMovies(@PathVariable int franchiseId) {
        Collection<MovieGetDTO> movies = movieMapper.movieToMovieGetDto(
                franchiseService.findAllMovies(franchiseId)
        );
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "Get characters of a franchise.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{franchiseId}/characters") // GET: localhost:8080/api/v1/franchises/1/characters
    public ResponseEntity<Collection<CharacterGetDTO>> findAllCharacters(@PathVariable int franchiseId) {
        Collection<CharacterGetDTO> characters = characterMapper.characterToCharacterGetDto(
                franchiseService.findAllCharacters(franchiseId)
        );
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "Delete a franchise.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(
                    responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/franchises/1
    public ResponseEntity<Void> deleteById(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Add a new franchise. Movies will be null.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Franchise successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST: localhost:8080/api/v1/franchises
    public ResponseEntity<?> add(@RequestBody FranchisePostDTO franchisePostDto) {
        Franchise franchise = franchiseMapper.franchisePostDtoToFranchise(franchisePostDto);
        Franchise newFranchise = franchiseService.add(franchise);
        URI location = URI.create("franchises/" + newFranchise.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update movies of a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movies of franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{franchiseId}/movies") // PUT: localhost:8080/api/v1/franchises/1/movies
    public ResponseEntity<?> updateMovies(@RequestBody int[] movieIds, @PathVariable int franchiseId) {
        franchiseService.updateMovies(movieIds, franchiseId);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update a franchise. Movies of this franchise won't change.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/franchises/1
    public ResponseEntity<?> update(@RequestBody FranchisePutDTO franchisePutDTO, @PathVariable int id) {
        // Validate if id in request body and path are identical
        if (id != franchisePutDTO.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchiseMapper.franchisePutDtoToFranchise(franchisePutDTO)
        );
        return ResponseEntity.noContent().build();
    }
}
