package de.experis.moviecharactersapi.controllers;

import de.experis.moviecharactersapi.mappers.CharacterMapper;
import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.dto.CharacterGetDTO;
import de.experis.moviecharactersapi.models.dto.CharacterPostDTO;
import de.experis.moviecharactersapi.models.dto.CharacterPutDTO;
import de.experis.moviecharactersapi.services.character.CharacterService;
import de.experis.moviecharactersapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all characters.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterGetDTO.class)))})
    })
    @GetMapping // GET: localhost:8080/api/v1/characters
    public ResponseEntity<Collection<CharacterGetDTO>> findAll() {
        Collection<CharacterGetDTO> characters = characterMapper.characterToCharacterGetDto(characterService.findAll());
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "Get a character by ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CharacterGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/characters/1
    public ResponseEntity<CharacterGetDTO> findById(@PathVariable int id) {
        CharacterGetDTO character = characterMapper.characterToCharacterGetDto(
                characterService.findById(id)
        );
        return ResponseEntity.ok(character);
    }

    @Operation(summary = "Delete a character.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(
                    responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/characters/1
    public ResponseEntity<?> deleteById(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Add a new character. Movies will be null.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Character successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST: localhost:8080/api/v1/characters
    public ResponseEntity<?> add(@RequestBody CharacterPostDTO characterPostDto) {
        Character character = characterMapper.characterPostDtoToCharacter(characterPostDto);
        Character newCharacter = characterService.add(character);
        URI location = URI.create("characters/" + newCharacter.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a character. Movies of this character won't change.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/characters/1
    public ResponseEntity<?> update(@RequestBody CharacterPutDTO characterPutDto, @PathVariable int id) {
        if (id != characterPutDto.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(
                characterMapper.characterPutDtoToCharacter(characterPutDto)
        );
        return ResponseEntity.noContent().build();
    }
}
