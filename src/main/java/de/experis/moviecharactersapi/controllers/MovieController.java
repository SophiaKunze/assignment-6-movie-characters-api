package de.experis.moviecharactersapi.controllers;

import de.experis.moviecharactersapi.mappers.CharacterMapper;
import de.experis.moviecharactersapi.mappers.MovieMapper;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.models.dto.CharacterGetDTO;
import de.experis.moviecharactersapi.models.dto.MovieGetDTO;
import de.experis.moviecharactersapi.models.dto.MoviePostDTO;
import de.experis.moviecharactersapi.models.dto.MoviePutDTO;
import de.experis.moviecharactersapi.services.movie.MovieService;
import de.experis.moviecharactersapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all movies.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MovieGetDTO.class)))})
    })
    @GetMapping // GET: localhost:8080/api/v1/movies
    public ResponseEntity<Collection<MovieGetDTO>> findAll() {
        Collection<MovieGetDTO> movies = movieMapper.movieToMovieGetDto(movieService.findAll());
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "Get a movie by ID.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieGetDTO.class))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/movies/1
    public ResponseEntity<MovieGetDTO> findById(@PathVariable int id) {
        MovieGetDTO movie = movieMapper.movieToMovieGetDto(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movie);
    }

    @Operation(summary = "Get characters of a movie.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterGetDTO.class)))}),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{movieId}/characters") // GET: localhost:8080/api/v1/movies/1/characters
    public ResponseEntity<Collection<CharacterGetDTO>> findAllCharacters(@PathVariable int movieId) {
        Collection<CharacterGetDTO> characters = characterMapper.characterToCharacterGetDto(
                movieService.findAllCharacters(movieId)
        );
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "Delete a movie.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/movies/1
    public ResponseEntity<?> deleteById(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Add a new movie. Franchise and characters will be null.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Movie successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })
    @PostMapping // POST: localhost:8080/api/v1/movies
    public ResponseEntity<?> add(@RequestBody MoviePostDTO moviePostDto) {
        Movie movie = movieMapper.moviePostDtoToMovie(moviePostDto);
        Movie newMovie = movieService.add(movie);
        URI location = URI.create("movies/" + newMovie.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update characters of a movie.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Characters of movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie or character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{movieId}/characters") // PUT: localhost:8080/api/v1/movies/1/characters
    public ResponseEntity<?> updateCharacters(@RequestBody int[] characterIds, @PathVariable int movieId) {
        movieService.updateCharacters(characterIds, movieId);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update a movie. Characters of this movie won't change.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/movies/1
    public ResponseEntity<?> update(@RequestBody MoviePutDTO MoviePutDto, @PathVariable int id) {
        // Validate if id in request body and path are identical
        if (id != MoviePutDto.getId())
            return ResponseEntity.badRequest().build();
        movieService.update(
                movieMapper.moviePutDtoToMovie(MoviePutDto)
        );
        return ResponseEntity.noContent().build();
    }
}
