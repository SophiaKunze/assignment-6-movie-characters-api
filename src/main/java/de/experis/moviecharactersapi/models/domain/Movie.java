package de.experis.moviecharactersapi.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Domain class (entity) to represent a Movie.
 * Includes an auto generated key and lombok getters and setters.
 * Relationships are configured as default, so collections are lazily loaded.
 */
@Getter
@Setter
@Entity
public class Movie {
    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String title;
    @Column(length = 200)
    private String genre;
    @Column
    private Integer year;
    @Column(length = 50)
    private String directorName;
    @Column(length = 200)
    private String posterUrl;
    @Column(length = 200)
    private String trailerUrl;

    // Relationships
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
    @ManyToMany(mappedBy = "movies")
    private Set<Character> characters;
}
