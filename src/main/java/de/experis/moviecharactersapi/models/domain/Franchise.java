package de.experis.moviecharactersapi.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Domain class (entity) to represent a Franchise.
 * Includes an auto generated key and lombok getters and setters.
 * Relationships are configured as default, so collections are lazily loaded.
 */
@Getter
@Setter
@Entity
public class Franchise {
    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 30, nullable = false)
    private String name;
    @Column(length = 200)
    private String description;

    // Relationships
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
