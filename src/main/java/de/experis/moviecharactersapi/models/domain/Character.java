package de.experis.moviecharactersapi.models.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Domain class (entity) to represent a Character.
 * Includes an auto generated key and lombok getters and setters.
 * Relationships are configured as default, so collections are lazily loaded.
 */
@Getter
@Setter
@Entity
public class Character {
    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @Column(length = 50)
    private String alias;
    @Column(length = 10)
    private String gender;
    @Column(length = 200)
    private String pictureUrl;

    // Relationships
    @ManyToMany
    private Set<Movie> movies;
}
