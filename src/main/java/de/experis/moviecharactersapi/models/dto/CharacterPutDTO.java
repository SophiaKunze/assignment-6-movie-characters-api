package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

@Data
public class CharacterPutDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;
}
