package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

import java.util.Set;

@Data
public class CharacterPostDTO {
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;
}
