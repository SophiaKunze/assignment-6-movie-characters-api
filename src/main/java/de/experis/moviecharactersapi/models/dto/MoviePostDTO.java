package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

@Data
public class MoviePostDTO {
    private String title;
    private String genre;
    private int year;
    private String directorName;
    private String posterUrl;
    private String trailerUrl;
}
