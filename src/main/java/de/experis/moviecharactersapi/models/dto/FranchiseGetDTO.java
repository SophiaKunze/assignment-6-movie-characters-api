package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseGetDTO {
    private int id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
