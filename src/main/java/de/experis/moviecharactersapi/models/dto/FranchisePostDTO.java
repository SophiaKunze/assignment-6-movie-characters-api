package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

@Data
public class FranchisePostDTO {
    private String name;
    private String description;
}
