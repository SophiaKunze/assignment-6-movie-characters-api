package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

@Data
public class FranchisePutDTO {
    private int id;
    private String name;
    private String description;
}
