package de.experis.moviecharactersapi.models.dto;

import lombok.Data;

@Data
public class MoviePutDTO {
    private int id;
    private String title;
    private String genre;
    private int year;
    private String directorName;
    private String posterUrl;
    private String trailerUrl;
    private int franchise;
}
