package de.experis.moviecharactersapi.mappers;

import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Franchise;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.models.dto.MovieGetDTO;
import de.experis.moviecharactersapi.models.dto.MoviePostDTO;
import de.experis.moviecharactersapi.models.dto.MoviePutDTO;
import de.experis.moviecharactersapi.services.character.CharacterService;
import de.experis.moviecharactersapi.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected FranchiseService franchiseService;

    // Method overloading (*1 and *2) allows to get single Movie or collection of Movies
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToCharacterIds")
    @Mapping(target = "franchise", source = "franchise.id")
    public abstract MovieGetDTO movieToMovieGetDto(Movie movie); // *1

    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToCharacterIds")
    @Mapping(target = "franchise", source = "franchise.id")
    public abstract Collection<MovieGetDTO> movieToMovieGetDto(Collection<Movie> movies); // *2

    @Mapping(target = "characters", ignore = true)
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    public abstract Movie moviePutDtoToMovie(MoviePutDTO moviePutDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "franchise", ignore = true)
    @Mapping(target = "characters", ignore = true)
    public abstract Movie moviePostDtoToMovie(MoviePostDTO moviePostDto);

    // Custom mappings

    @Named("franchiseIdToFranchise")
    Franchise mapFranchiseIdToFranchise(int id) {
        // if no franchise id given
        if (id == 0) {
            return null;
        }
        return franchiseService.findById(id);
    }

    @Named("charactersToCharacterIds")
    Set<Integer> mapCharactersToCharactersIds(Set<Character> characters) {
        if (characters == null)
            return null;
        return characters.stream()
                .map(Character::getId)
                .collect(Collectors.toSet());
    }
}
