package de.experis.moviecharactersapi.mappers;

import de.experis.moviecharactersapi.models.domain.Franchise;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.models.dto.FranchiseGetDTO;
import de.experis.moviecharactersapi.models.dto.FranchisePostDTO;
import de.experis.moviecharactersapi.models.dto.FranchisePutDTO;
import de.experis.moviecharactersapi.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {

    @Autowired
    protected MovieService movieService;

    // Method overloading (*1 and *2) allows to get single Franchise or collection of Franchises
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToMovieIds")
    public abstract FranchiseGetDTO franchiseToFranchiseGetDto(Franchise franchise); // *1

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToMovieIds")
    public abstract Collection<FranchiseGetDTO> franchiseToFranchiseGetDto(Collection<Franchise> franchise); // *2

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "movies", ignore = true)
    public abstract Franchise franchisePostDtoToFranchise(FranchisePostDTO franchisePostDto);

    @Mapping(target = "movies", ignore = true)
    public abstract Franchise franchisePutDtoToFranchise(FranchisePutDTO franchisePutDto);

    @Named("moviesToMovieIds")
    Set<Integer> mapMoviesToMovieIds(Set<Movie> movies) {
        if (movies == null)
            return null;
        return movies.stream()
                .map(Movie::getId)
                .collect(Collectors.toSet());
    }
}
