package de.experis.moviecharactersapi.mappers;

import de.experis.moviecharactersapi.models.domain.Character;
import de.experis.moviecharactersapi.models.domain.Movie;
import de.experis.moviecharactersapi.models.dto.CharacterGetDTO;
import de.experis.moviecharactersapi.models.dto.CharacterPostDTO;
import de.experis.moviecharactersapi.models.dto.CharacterPutDTO;
import de.experis.moviecharactersapi.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    protected MovieService movieService;

    // Method overloading (*1 and *2) allows to get single Character or collection of Characters
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToMovieIds")
    public abstract CharacterGetDTO characterToCharacterGetDto(Character character); // *1

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToMovieIds")
    public abstract Collection<CharacterGetDTO> characterToCharacterGetDto(Collection<Character> characters); // *2


    @Mapping(target = "movies", ignore = true)
    public abstract Character characterPutDtoToCharacter(CharacterPutDTO characterPutDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "movies", ignore = true)
    public abstract Character characterPostDtoToCharacter(CharacterPostDTO characterPostDto);

    // Custom mappings

    @Named("moviesToMovieIds")
    Set<Integer> mapMoviesToMovieIds(Set<Movie> movies) {
        if (movies == null)
            return null;
        return movies.stream()
                .map(Movie::getId)
                .collect(Collectors.toSet());
    }

}
